<?php
/* Implementation of hook_menu() */
function feedme_menu($may_cache) {
  $items = array();
  
  if ($may_cache) {
    $items[] = array(
      'path' => 'feedme',
      'access' => user_access('administer feedme'),
      'callback' => '_feedme_router',
      'type' => MENU_CALLBACK,
    );
    // should really refactor the following into the router.  For now it works though.
    $items[] = array(
      'path' => 'admin/settings/feedme',
      'title' => t('FeedMe module settings'),
      'description' => t('Adjust global settings for the FeedMe module.'),
      'callback' => 'drupal_get_form',
      'callback arguments' => 'feedme_admin',
      'access' => user_access('administer feedme'),
      'type' => MENU_NORMAL_ITEM,
    );
    $items[] = array(
      'path' => 'feedme/addfeed',
      'title' => t('Add New FeedMe Feed'),
      'callback' => 'drupal_get_form',
      'callback arguments' => 'feedme_addfeed_form',
      'access' => user_access('create feedme feeds'),
      'type' => MENU_NORMAL_ITEM,
    );
  }
  return $items;
}

/* menu router */
function _feedme_router() {
  $args = func_get_args();
  
  // Default is to display form if no args passed
  if (!count($args)) {
    $args[0] = 'feeds';
    $args[1] = 'list';
  }
  
  switch ($args[0]) {
    case 'feeds':
      if (!isset($args[1]) || !is_numeric($args[1])) {
        if ($args[1] == 'list') {
					// url: feedme/feeds/list
          // return a table containing the list of feeds
          drupal_set_title(t('Feeds List'));
          return _feedme_feedlist();
        }
				else {
          drupal_not_found();
          return;
        }
      }
      // else $args[1] isset and is_numeric
      # url: feedme/feeds/$fid/$args[2]
      drupal_set_title(t("Feed # $args[1]"));
      // switch between feed "arguments", used here as commands, like 'refresh' or 'destroy'.
      switch ($args[2]) {
        case 'refresh':
          return _feedme_refresh_feeditems($args[1]);
        case 'destroy':
          return _feedme_destroy_feeditems($args[1]);
        default: // if no argument set, then we just display the feed_table.
          return theme('feedme_table', $args[1]);
      }
  }
  drupal_not_found();
  return;
}
