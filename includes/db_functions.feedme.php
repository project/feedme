<?php
/* Creates the values table for the $fid and then automatically populates the $feeditems into it.
 *  It uses the function _feedme_populate_values_table() to do that.  If you want to just populate the data and not
 *     recreate the table itself, you may call that function instead.
 *
 *  Returns $group_id, which is a count from the _feedme_populate_values_table() function - count of number of records added.
 */
function _feedme_create_values_table($fid, $feeditems) {
  // create basic table skeleton (does not include the keys yet.)
  db_query("
    CREATE TABLE IF NOT EXISTS {feedme}_values_$fid (
      nid int(10) unsigned NOT NULL default '0',
      id INT(10) NOT NULL,
      feed_id INT(10) NOT NULL,
      PRIMARY KEY (nid),
      KEY `feedme_values_nid` (nid)
      )
  ");
  drupal_set_message("<em>feedme_values_$fid</em> table created.");

  // start with the keys, so we can build the table columns.
  $keys = array();
  foreach ($feeditems as $feeditem) {
    $keys = array();
    foreach ($feeditem as $key => $value) {
      $keys[] = $key;
    }
  }
  foreach ($keys as $key) { // loop through the keys and create the columns
    db_query("ALTER TABLE {feedme}_values_$fid ADD $key VARCHAR( 255 ) NOT NULL");
  }

  // now we can populate the values_table
  $group_id = _feedme_populate_values_table($fid, $feeditems, $keys);

  return $group_id;
}

/**
 * Implementation of hook_insert().
 *
 * As a new node is being inserted into the database, we need to do our own
 * database inserts.
 */
#function feedme_insert($node) {
//  db_query("INSERT INTO {node_example} (vid, nid, color, quantity) VALUES (%d, %d, '%s', %d)", $node->vid, $node->nid, $node->color, $node->quantity);
#}

function _feedme_populate_values_table($fid, $feeditems) {
  // get the keys so we can use them later.  :)
  $keys = _feedme_get_keys($fid);
  // get last node id... somehow.
  $nid = 999;

  foreach ($feeditems as $dkey => $feeditem) {
    $nid += 1;
    $group_id = $dkey + 1; // add 1 to dkey to get group_id
    $query = ''; // clear out the query each time.  :)
    $query = "INSERT INTO {feedme}_values_$fid (`id`, `feed_id`, `";
    $query .= implode("`,`", $keys);
    $query .= "`) VALUES ('$group_id', '$fid', '$nid', ";
    foreach ($feeditem as $key => $value) {
      $query .= "'check_plain($value)',";
    }
    $query = rtrim($query, ',');
    $query .= ");";
    db_query($query);
  }
  return $group_id;
}

/*
 *  Empties out the values_table_$fid
 *  @param $fid = feed id
 */
function _feedme_empty_values_table($fid) {
  $query = "TRUNCATE TABLE {feedme}_values_$fid";
  $result = db_query($query);
  if ($result) {
    drupal_set_message("<em>feedme_values_$fid</em> table emptied.");
  }
  else {
    drupal_set_message("There was an error clearing the table.  Please try again.", 'error');
  }
}

/* Completely destroys the values_table_$fid.
 *  If you just want to empty the table, please call _feedme_empty_values_table() instead, as that will keep the table structure in tact.
 */
function _feedme_drop_values_table($fid) {
  $query = "DROP TABLE {feedme}_values_$fid";
  $active_fid = variable_get('feedme_fid', 1);
  
  if ($fid == $active_fid) {
    drupal_set_message("Unable to remove current feed.  Please change the Current Feed ID before attempting to delete this feed.", 'error');
    return 0;
  }
  else {
    $result = db_query($query);
    if ($result) {
      db_query("DELETE FROM {feedme} WHERE id = %d", $fid); // delete the link in the feedme table too.
      drupal_set_message("<em>feedme_values_$fid</em> table destroyed.");
      return 1;
    }
    else {
      drupal_set_message("There was an error destroying the table.  Please try again.", 'error');
      return 0;
    }
  }
}


function _feedme_get_keys($fid) {
  $cnames = array();
  $result = db_query("SHOW COLUMNS FROM {feedme}_values_$fid");
  $count = 0;
  while ($row = mysql_fetch_row($result)) {
    $cnt = 0;
    foreach ($row as $item) {
      if ($cnt == 0) {
        if ($item == 'id' || $item == 'feed_id') {
          # don't have to do anything here, since we don't want to return those values
        }
        else {
          $cnames[$count] = $item;
        }
        $cnt++;
        $count++;
      }
    }
  }
  return $cnames;
}
