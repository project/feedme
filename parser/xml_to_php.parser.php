<?php
class FeedItem {
  function FeedItem($items) {
    foreach ($items as $k => $v) {
      $this->$k = $items[$k];
    }
  }
}

/* this magical function reads the XML file and generates an array of arrays of FeedItem's */
function readDatabase($filename, $feed_key=NULL) {
  // read the XML database of tour items
	if (!$feed_key) {
		$feed_key = variable_get('feedme_default_feedkey', '');
	}
  $data = implode("", file($filename));
  $parser = xml_parser_create();
  xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
  xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
  xml_parse_into_struct($parser, $data, $values, $tags);
  xml_parser_free($parser);

  // loop through the structures
  foreach ($tags as $key => $val) {
    if ($key == $feed_key) {
      $ranges = $val;
      // each contiguous pair of array entries are the
      // lower and upper range for each show definition
      for ($i=0; $i < count($ranges); $i+=2) {
        $offset = $ranges[$i] + 1;
        $len = $ranges[$i + 1] - $offset;
        $tdb[] = parseFeed(array_slice($values, $offset, $len));
      }
    }
		else {
      continue;
    }
  }
  return $tdb;
}

function parseFeed($values) {
  for ($i=0; $i < count($values); $i++) {
    $items[$values[$i]["tag"]] = $values[$i]["value"];
  }
  return new FeedItem($items);
}
