Created 5/2008 by Justus Boatright for Word Records.

This module is designed to work with the XML tour feeds generated from Word Records, allowing direct access
within Drupal.  I created this module out of necessity to work with the particular feed format.
An example of the XML feed that was used to create this module can be found in the example.xml file.

FeedMe requires Views 1.x in order to work properly (although that is debatable until the next version comes out, 
but really, you should be using views already.)

Realistically this module can be used to generate arrays for practically any XML-formatted feed.  This is probably
NOT the best module for RSS/Atom feeds (FeedAPI or Aggregator are quite a bit more robust for that.),
but if you have a "non-standard" xml feed that you need to work with, this module should be perfect.
